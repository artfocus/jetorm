<?php

namespace Model\Repositories;

use Artfocus\JetORM;
use Model;

/** @entity Model\Entities\Author */
class AuthorRepository extends JetORM\Repository
{

	/**
	 * @param int $id
	 * @return Model\Entities\Author
	 */
	public function getByID($id)
	{
		return new Model\Entities\Author($this->getTable()->get($id));
	}

}
