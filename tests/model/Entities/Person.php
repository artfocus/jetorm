<?php

namespace Model\Entities;

use Artfocus\JetORM;
use Nette;

/**
 * @property-read int|null $id
 * @property string $name
 * @property-read Nette\Utils\DateTime|null $born
 */
abstract class Person extends JetORM\Entity
{

	/**
	 * @return string
	 */
	public function getName()
	{
		return Nette\Utils\Strings::capitalize($this->row->name);
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->row->name = $name;
		return $this;
	}

}
