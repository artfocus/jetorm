<?php

namespace Model\Entities;

use Artfocus\JetORM\Entity;

/**
 * @property-read int|null $id
 * @property float $price
 * @property float $vat
 * @property float $total
 */
class Price extends Entity
{

}
