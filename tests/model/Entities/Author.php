<?php

namespace Model\Entities;

use Artfocus\JetORM;

/**
 * @property string $web
 * @property mixed|null $customData
 *
 * @method string getWeb()
 * @method Author setWeb()
 */
class Author extends Person
{

	/** @return JetORM\Collection */
	public function getBooks()
	{
		return $this->getMany('Model\Entities\Book', 'book');
	}

	/**
	 * @param mixed $data
	 * @return $this
	 */
	public function setCustomData($data)
	{
		return $this->setEntityValue($data);
	}

	/**
	 * @return mixed
	 */
	public function getCustomData()
	{
		return $this->getEntityValue();
	}

}
