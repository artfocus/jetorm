<?php

namespace Model;

use Model\Repositories;
use Model\Services;
use Nette;

class ServiceLocator
{

	/** @var Nette\Caching\Storages\FileStorage */
	private static $cacheStorage = NULL;

	/** @var Nette\Database\Context */
	private static $dbContext = NULL;

	/** @var Repositories\BookRepository */
	private static $bookRepository = NULL;

	/** @var Repositories\AuthorRepository */
	private static $authorRepository = NULL;

	/** @var Services\BookService */
	private static $bookService = NULL;

	/** @return Nette\Caching\Storages\FileStorage */
	public static function getCacheStorage()
	{
		if (self::$cacheStorage === NULL) {
			self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../temp');
		}

		return self::$cacheStorage;
	}

	/** @return Nette\Database\Context */
	public static function getDbContext()
	{
		if (self::$dbContext === NULL) {
			self::$dbContext = new Nette\Database\Context(
				$connection = new Nette\Database\Connection('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS),
				$structure = new Nette\Database\Structure($connection, self::getCacheStorage()),
				new Nette\Database\Conventions\DiscoveredConventions($structure),
				self::getCacheStorage()
			);
		}

		return self::$dbContext;
	}

	/** @return Repositories\BookRepository */
	public static function getBookRepository()
	{
		if (self::$bookRepository === NULL) {
			self::$bookRepository = new Repositories\BookRepository(self::getDbContext(), __DIR__ . '/books');
		}

		return self::$bookRepository;
	}

	/**
	 * @param string $file
	 */
	public static function loadFixture($file)
	{
		$connection = self::getDbContext()->getConnection();
		Nette\Database\Helpers::loadFromFile($connection, $file);
	}

	/** @return Repositories\AuthorRepository */
	public static function getAuthorRepository()
	{
		if (self::$authorRepository === NULL) {
			self::$authorRepository = new Repositories\AuthorRepository(self::getDbContext());
		}

		return self::$authorRepository;
	}

	/** @return Services\BookService */
	public static function getBookFacade()
	{
		if (self::$bookService === NULL) {
			self::$bookService = new Services\BookService(self::getBookRepository());
		}

		return self::$bookService;
	}

}
