<?php

namespace Tests;

use Artfocus\JetORM\Collection;
use Artfocus\JetORM\LazyCollection;

class LazyCollectionTest extends \PHPUnit_Framework_TestCase
{

	public function testInterface()
	{
		$collection = $this->createCollection();

		$collection->expects($this->once())
			->method('count')
			->willReturn(5);

		$collection->expects($this->once())
			->method('countDistinct')
			->with('id')
			->willReturn(4);

		$lazy = new LazyCollection($collection, 3);

		$this->assertSame($lazy, $lazy->orderBy('id'));
		$this->assertSame($lazy, $lazy->limit(1, 2));
		$this->assertSame(5, $lazy->count());
		$this->assertSame(4, $lazy->countDistinct('id'));
	}

	public function test1()
	{
		$collection = $this->createCollection();

		$collection->expects($this->exactly(3))
			->method('limit')
			->withConsecutive(
				[3, 0],
				[3, 3],
				[3, 6]
			);

		$collection->expects($this->exactly(3))
			->method('toArray')
			->willReturnOnConsecutiveCalls(
				['A', 'B', 'C'],
				['D', 'E', 'F'],
				['G', 'H']
			);

		$lazy = new LazyCollection($collection, 3);
		$items = [];

		foreach ($lazy as $key => $item) {
			$items[$key] = $item;
		}

		$this->assertSame([
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
		], $items);
	}

	public function test2()
	{
		$collection = $this->createCollection();

		$collection->expects($this->exactly(2))
			->method('limit')
			->withConsecutive(
				[3, 0],
				[3, 0]
			);

		$collection->expects($this->exactly(2))
			->method('toArray')
			->willReturnOnConsecutiveCalls(
				['A', 'B'],
				['A', 'B']
			);

		$lazy = new LazyCollection($collection, 3);
		$items = [];

		// first iteration
		foreach ($lazy as $key => $item) {
			$items[$key] = $item;
		}

		// second iteration
		foreach ($lazy as $key => $item) {
			$items[$key] = $item;
		}

		$this->assertSame([
			'A',
			'B',
		], $items);
	}

	public function testDelegation()
	{
		$collection = $this->createCollection();

		$collection->expects($this->exactly(3))
			->method('limit')
			->withConsecutive(
				[1, 1],
				[3, 2],
				[1, 5]
			);

		$collection->expects($this->exactly(3))
			->method('toArray')
			->willReturnOnConsecutiveCalls(
				['X'],
				['A', 'B', 'C'],
				['D']
			);

		$lazy = new LazyCollection($collection, 3);

		// first limit
		$items = [];
		$lazy->limit(1, 1);
		foreach ($lazy as $key => $item) {
			$items[$key] = $item;
		}

		$this->assertSame([
			'X',
		], $items);

		// limit changed
		$items = [];
		$lazy->limit(4, 2);
		foreach ($lazy as $key => $item) {
			$items[$key] = $item;
		}

		$this->assertSame([
			'A',
			'B',
			'C',
			'D',
		], $items);
	}

	/**
	 * @return \PHPUnit_Framework_MockObject_MockObject|Collection
	 */
	private function createCollection()
	{
		return $this->getMock('\Artfocus\JetORM\Collection');
	}

}
