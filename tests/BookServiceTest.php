<?php

namespace Tests;

use Model\ServiceLocator;
use Nette;

class BookServiceTest extends \PHPUnit_Framework_TestCase
{

	protected function setUp()
	{
		parent::setUp();

		ServiceLocator::loadFixture(__DIR__ . '/model/db/db.sql');
	}

	public function testLatest()
	{
		$books = [];
		foreach (ServiceLocator::getBookFacade()->getLatest() as $book) {
			$books[] = $book->toArrayRecursive(1);
		}

		$this->assertEquals([
			[
				'id' => 1,
				'bookTitle' => '1001 tipu a triku pro PHP',
				'author' => [
					'id' => 11,
					'born' => NULL,
					'name' => 'Jakub Vrana',
					'web' => 'http://www.vrana.cz/',
					'customData' => NULL,
					'books' => [],
				],
				'written' => new Nette\Utils\DateTime('2010-01-01'),
				'available' => TRUE,
				'tags' => [
					[
						'id' => 21,
						'name' => 'PHP',
					],
					[
						'id' => 22,
						'name' => 'MySQL',
					],
				],
			],
			[
				'id' => 2,
				'bookTitle' => 'JUSH',
				'author' => [
					'id' => 11,
					'born' => NULL,
					'name' => 'Jakub Vrana',
					'web' => 'http://www.vrana.cz/',
					'customData' => NULL,
					'books' => [],
				],
				'written' => new Nette\Utils\DateTime('2007-01-01'),
				'available' => TRUE,
				'tags' => [
					[
						'id' => 23,
						'name' => 'JavaScript',
					],
				],
			],
			[
				'id' => 4,
				'bookTitle' => 'Dibi',
				'author' => [
					'id' => 12,
					'born' => NULL,
					'name' => 'David Grudl',
					'web' => 'http://davidgrudl.com/',
					'customData' => NULL,
					'books' => [],
				],
				'written' => new Nette\Utils\DateTime('2005-01-01'),
				'available' => TRUE,
				'tags' => [
					[
						'id' => 21,
						'name' => 'PHP',
					],
					[
						'id' => 22,
						'name' => 'MySQL',
					],
				],
			],

		], $books);
	}

}
