<?php

namespace Tests;

use Artfocus\JetORM\Collection;
use Model\Entities\Author;
use Nette;
use Nette\Database\Connection as NConnection;
use Nette\Database\ResultSet as NResultSet;
use Model\ServiceLocator;

class BookRepositoryTest extends \PHPUnit_Framework_TestCase
{

	protected function setUp()
	{
		parent::setUp();

		ServiceLocator::loadFixture(__DIR__ . '/model/db/db.sql');
	}

	public function testEntity()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);

		$this->assertInstanceOf('Model\Entities\Book', $book);

		$expected = [
			'id' => 1,
			'bookTitle' => '1001 tipu a triku pro PHP',
			'author' => [
				'id' => 11,
				'born' => NULL,
				'name' => 'Jakub Vrana',
				'web' => 'http://www.vrana.cz/',
				'customData' => NULL,
				'books' => [],
			],
			'written' => new Nette\Utils\DateTime('2010-01-01'),
			'available' => TRUE,
			'tags' => [
				[
					'id' => 21,
					'name' => 'PHP',
				],
				[
					'id' => 22,
					'name' => 'MySQL',
				],
			],
		];

		$this->assertEquals($expected, $book->toArrayRecursive(1));
	}

	public function testManyToOne()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);
		$author = $book->getAuthor();
		$this->assertEquals('Jakub Vrana', $author->getName());
	}

	public function testManyToMany()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);
		$tags = [];
		foreach ($book->getTags() as $tag) {
			$tags[] = $tag->name;
		}

		$this->assertEquals(['PHP', 'MySQL'], $tags);
	}

	public function testSearch()
	{
		$books = [];
		foreach (ServiceLocator::getBookRepository()->getByTag('PHP') as $book) {
			$books[] = $book->bookTitle;
		}

		$this->assertEquals(['1001 tipu a triku pro PHP', 'Nette', 'Dibi'], $books);
	}

	public function testCount()
	{
		$allBooks = ServiceLocator::getBookRepository()->getAll();
		$bookTags = ServiceLocator::getBookRepository()->getByID(3)->getTags();

		$this->assertEquals(4, count($allBooks->limit(2))); // data not received yet -> count as non-limited
		$this->assertEquals(2, count($allBooks->limit(2)->toArray())); // data received
		$this->assertEquals(1, count($bookTags));
	}

	/** @see http://phpfashion.com/mvc-paradox-a-jak-jej-resit */
	public function testPresenterFlow()
	{
		// load data
		$books = ServiceLocator::getBookRepository()->getAll();

		// paginate result
		$paginator = new Nette\Utils\Paginator;
		$paginator->itemsPerPage = 2;
		$paginator->itemCount = count($books);
		$paginator->page = 2;

		$books->limit($paginator->length, $paginator->offset);

		// render them ordered in template
		$array = [];
		foreach ($books->orderBy('book_title') as $book) {
			$array[] = $book->bookTitle;
		}

		$this->assertEquals(['JUSH', 'Nette'], $array);
	}

	/** Tests equality of queries using native & ORM data access */
	public function testQueries()
	{
		$context = ServiceLocator::getDbContext();

		$context->getConnection()->onQuery['queryDump'] = function (NConnection $c, NResultSet $r) {
			echo $r->getQueryString() . "\n";
		};

		ob_start();

		foreach ($context->table('book') as $book) {
			foreach ($book->related('book_tag')->order('tag.name DESC') as $bookTag) {
				echo $bookTag->tag->name, ', ';
			}
		}

		$native = ob_get_clean();

		ob_start();

		foreach (ServiceLocator::getBookRepository()->getAll() as $book) {
			foreach ($book->getTags()->orderBy('tag.name', Collection::DESC) as $tag) {
				echo $tag->name, ', ';
			}
		}

		$repository = ob_get_clean();

		unset($context->getConnection()->onQuery['queryDump']);
		$this->assertEquals($native, $repository);
	}

	public function testCreate()
	{
		$bookRepository = ServiceLocator::getBookRepository();
		$authorRepository = ServiceLocator::getAuthorRepository();

		/** @var Author $author */
		$author = $authorRepository->create();
		$author->setWeb('http://example.com/abc');
		$author->setName('Invalida Lada');

		$book = $bookRepository->createBook();
		$book->setBookTitle('ABC');
		$book->setAuthor($author);
		$book->setWritten(new Nette\Utils\DateTime('2008-01-01'));

		$authorRepository->persist($book->getAuthor());
		$bookRepository->persist($book);

		$this->assertGreaterThan(0, $book->getId());
		$this->assertGreaterThan(0, $book->getAuthor()->getId());
	}

	public function testCreateAndUpdate()
	{
		// === CREATION

		$repo = ServiceLocator::getBookRepository();

		$book = $repo->createBook();
		$book->bookTitle = 'Texy 2';
		$book->setAuthor(ServiceLocator::getAuthorRepository()->getByID(12));
		$book->written = new Nette\Utils\DateTime('2008-01-01');
		$repo->persist($book);

		$expected = [
			'author' => [
				'id' => 12,
				'born' => NULL,
				'name' => 'David Grudl',
				'web' => 'http://davidgrudl.com/',
				'customData' => NULL,
				'books' => [],
			],
			'id' => 5,
			'bookTitle' => 'Texy 2',
			'written' => new Nette\Utils\DateTime('2008-01-01'),
			'available' => TRUE,
			'tags' => [],

		];

		$this->assertEquals($expected, $book->toArrayRecursive(1));

		$this->assertEquals('David Grudl', $book->getAuthor()->getName());

		// === UPDATE

		$repo = ServiceLocator::getBookRepository();

		// change title
		$book = $repo->getByID(5);
		$book->bookTitle = 'New title';
		$this->assertEquals('New title', $book->bookTitle);

		$rows = $repo->persist($book);
		$this->assertEquals(1, $rows);
		$this->assertEquals('New title', $book->bookTitle);

		// change author
		$author = ServiceLocator::getAuthorRepository()->getByID(13);
		$this->assertEquals('Geek', $author->getName());

		$this->assertEquals('David Grudl', $book->getAuthor()->getName());
		$book->setAuthor($author);
		$rows = $repo->persist($book);

		$this->assertEquals(1, $rows);
		$this->assertEquals('Geek', $book->getAuthor()->getName());

		// change availability
		$book->available = FALSE;
		$repo->persist($book);
		$this->assertFalse($book->available);

		$this->assertEquals([
			'author' => [
				'id' => 13,
				'born' => NULL,
				'name' => 'Geek',
				'web' => 'http://example.com',
				'customData' => NULL,
				'books' => [],
			],
			'id' => 5,
			'bookTitle' => 'New title',
			'written' => new Nette\Utils\DateTime('2008-01-01'),
			'available' => FALSE,
			'tags' => [],
		], $book->toArrayRecursive(1));
	}

	public function testDelete()
	{
		$repo = ServiceLocator::getBookRepository();
		$this->assertEquals(4, count($repo->getAll()));

		$this->assertTrue($repo->delete($repo->getByID(4)));
		$this->assertEquals(3, count($repo->getAll()));
	}

}
