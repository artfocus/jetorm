<?php

namespace Tests;

use Model\ServiceLocator;
use Nette;

class CreationTest extends \PHPUnit_Framework_TestCase
{

	protected function setUp()
	{
		parent::setUp();

		ServiceLocator::loadFixture(__DIR__ . '/model/db/db.sql');
	}

	public function testCreation()
	{
		$repo = ServiceLocator::getBookRepository();

		$book = $repo->createBook();
		$book->setAuthor(ServiceLocator::getAuthorRepository()->getByID(11));
		$book->bookTitle = 'Brand new book';

		$rows = ServiceLocator::getBookRepository()->persist($book);
		$this->assertEquals(1, $rows);

		// default values
		$this->assertEquals($book->available, TRUE);
		$this->assertEquals($book->written, NULL);
	}

	public function testAddTags()
	{
		$repo = ServiceLocator::getBookRepository();

		$book = $repo->createBook();
		$book->bookTitle = 'Testing book';
		$book->setAuthor(ServiceLocator::getAuthorRepository()->getByID(11));
		$book->addTag('PHP');
		$book->addTag('Nový tag');

		ServiceLocator::getBookRepository()->persist($book);

		$expected = [
			[
				'id' => 21,
				'name' => 'PHP',
			],
			[
				'id' => 25,
				'name' => 'Nový tag',
			],
		];

		$actual = [];
		foreach ($book->getTags() as $tag) {
			$actual[] = $tag->toArray();
		}

		$this->assertEquals($expected, $actual);
	}

	public function testRemoveTags()
	{
		$repo = ServiceLocator::getBookRepository();
		$book = $repo->getByID(4);

		$book->removeTag('Nový tag');
		$repo->persist($book);

		$book->removeTag('MySQL');
		$repo->persist($book);

		$expected = [
			[
				'id' => 21,
				'name' => 'PHP',
			],
		];

		$actual = [];
		foreach ($book->getTags() as $tag) {
			$actual[] = $tag->toArray();
		}

		$this->assertEquals($expected, $actual);
	}

	public function testSerialization1()
	{
		$repo = ServiceLocator::getAuthorRepository();
		$author = $repo->getByID(13);

		$serialized = serialize($author);

		/** @var \Model\Entities\Author $unserialized */
		$unserialized = unserialize($serialized);

		$this->assertEquals('Geek', $unserialized->getName());
		$this->assertEquals('http://example.com', $unserialized->getWeb());
		$this->assertSame([], $unserialized->getBooks());
	}

	public function testSerialization2()
	{
		$repo = ServiceLocator::getAuthorRepository();
		$author = $repo->getByID(12);
		$author->setCustomData('abc123');

		$serialized = serialize($author);

		/** @var \Model\Entities\Author $unserialized */
		$unserialized = unserialize($serialized);

		$this->assertEquals('David Grudl', $unserialized->getName());
		$this->assertEquals('http://davidgrudl.com/', $unserialized->getWeb());
		$this->assertEquals('abc123', $unserialized->getCustomData());
	}

}
