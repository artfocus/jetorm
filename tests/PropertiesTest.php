<?php

namespace Tests;

use Artfocus\JetORM;
use Model;
use Model\ServiceLocator;
use Nette;

class PropertiesTest extends \PHPUnit_Framework_TestCase
{

	protected function setUp()
	{
		parent::setUp();

		ServiceLocator::loadFixture(__DIR__ . '/model/db/db.sql');
	}

	public function testSetters()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);

		// set as property
		$book->bookTitle = 'New title';
		$this->assertEquals('New title', $book->bookTitle);

		// setting read-only property
		try {
			$book->id = 123;
			$this->fail();

		} catch (JetORM\Exception\MemberAccessException $e) {
			if ($e->getMessage() !== 'Cannot write to an undeclared property Model\Entities\Book::$id.') {
				throw $e;
			}
		}

		// setting invalid type
		try {
			$book->bookTitle = 123;
			$this->fail();

		} catch (JetORM\Exception\InvalidArgumentException $e) {
			if ($e->getMessage() !== "Invalid type - 'string' expected, 'integer' given.") {
				throw $e;
			}
		}

		try {
			$book->available = 'TRUE';
			$this->fail();

		} catch (JetORM\Exception\InvalidArgumentException $e) {
			if ($e->getMessage() !== "Invalid type - 'boolean' expected, 'string' given.") {
				throw $e;
			}
		}

		// setting undeclared property
		try {
			$book->asdf = 'Book title';
			$this->fail();

		} catch (JetORM\Exception\MemberAccessException $e) {
			if ($e->getMessage() !== 'Cannot write to an undeclared property Model\Entities\Book::$asdf.') {
				throw $e;
			}
		}
	}

	public function testGetters()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);

		// get as property
		$this->assertEquals('1001 tipu a triku pro PHP', $book->bookTitle);

		// test type
		$this->assertTrue(is_int($book->id));
		$this->assertTrue(is_string($book->bookTitle));
		$this->assertTrue(is_bool($book->available));

		// getting undeclared property
		try {
			$book->asdf;
			$this->fail();

		} catch (JetORM\Exception\MemberAccessException $e) {
			if ($e->getMessage() !== 'Cannot read an undeclared property Model\Entities\Book::$asdf.') {
				throw $e;
			}
		}
	}

	public function testMagicCall()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);
		$value = $book->setBookTitle('new test title');

		$this->assertSame($book, $value);
		$this->assertEquals('new test title', $book->getBookTitle());
		$this->assertEquals('new test title', $book->bookTitle);
		$this->assertEquals('/1.jpg', $book->getImagePath());
		$this->assertEquals('/1.jpg', $book->imagePath);

		try {
			$book->setBookTitle(NULL);
			$this->fail();
		} catch (JetORM\Exception\InvalidArgumentException $e) {
			// ok
		}

		$this->assertEquals('new test title', $book->getBookTitle());
	}

	public function testMagicCall2()
	{
		$dateTime = Nette\Utils\DateTime::from('2014-06-14');

		$book = ServiceLocator::getBookRepository()->getByID(1);
		$value = $book->setWritten($dateTime);

		$this->assertSame($book, $value);
		$this->assertEquals($dateTime, $book->getWritten());
		$this->assertEquals($dateTime, $book->written);

		// test NULL
		$book->setWritten();
		$this->assertNull($book->getWritten());
		$this->assertNull($book->written);
	}

	public function testIsSet()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);

		// properties
		$this->assertTrue(isset($book->id));
		$this->assertTrue(isset($book->bookTitle));
		$this->assertFalse(isset($book->foo));

		// methods
		$this->assertTrue(isset($book->author));
		$this->assertTrue(isset($book->tags));

		// NULL values
		$book->written = NULL;
		$this->assertFalse(isset($book->written));

		$book->written = new Nette\Utils\DateTime;
		$this->assertTrue(isset($book->written));
	}

	public function testUnset()
	{
		try {
			$book = ServiceLocator::getBookRepository()->getByID(1);
			unset($book->author);
			$this->fail();

		} catch (JetORM\Exception\NotSupportedException $e) {
			// ok
		}
	}

	public function testToArray()
	{
		$book = ServiceLocator::getBookRepository()->getByID(2);

		$array = $book->toArray();
		$this->assertSame(2, $array['id']);
		$this->assertEquals('JUSH', $array['bookTitle']);
		$this->assertTrue($array['available']);
		$this->assertInternalType('array', $array['tags']);

		$this->assertInstanceOf('Nette\Utils\DateTime', $array['written']);
		$this->assertInstanceOf('Model\Entities\Author', $array['author']);
		$this->assertInstanceOf('Model\Entities\Tag', $array['tags'][0]);
	}

	public function testToArrayRecursive()
	{
		$book = ServiceLocator::getBookRepository()->getByID(2);

		$expected = [
			'id' => 2,
			'bookTitle' => 'JUSH',
			'author' => [
				'id' => 11,
				'born' => NULL,
				'name' => 'Jakub Vrana',
				'web' => 'http://www.vrana.cz/',
				'customData' => NULL,
				'books' => [],
			],
			'written' => new Nette\Utils\DateTime('2007-01-01'),
			'available' => TRUE,
			'tags' => [
				[
					'id' => 23,
					'name' => 'JavaScript',
				],
			],
		];

		$this->assertEquals($expected, $book->toArrayRecursive(1));
	}

	public function testInheritance()
	{
		$author = ServiceLocator::getAuthorRepository()->getByID(11);

		$this->assertEquals([
			'id' => 11,
			'born' => NULL,
			'name' => 'Jakub Vrana',
			'web' => 'http://www.vrana.cz/',
			'customData' => NULL,
			'books' => [],
		], $author->toArrayRecursive(0));
	}

	public function testClassTypes()
	{
		$book = ServiceLocator::getBookRepository()->getByID(1);
		$this->assertInstanceOf('DateTime', $book->written);
	}

	public function testNullable()
	{
		$repo = ServiceLocator::getBookRepository();

		$book = $repo->getByID(1);
		$book->written = NULL;
		$this->assertNull($book->written);

		$repo->persist($book);
		$this->assertNull($book->written);

		$book->written = new Nette\Utils\DateTime('1990-01-01');
		$this->assertEquals(new Nette\Utils\DateTime('1990-01-01'), $book->written);

		$repo->persist($book);
		$this->assertEquals(new Nette\Utils\DateTime('1990-01-01'), $book->written);

		$book->written = NULL;
		$this->assertNull($book->written);

		$repo->persist($book);
		$this->assertNull($book->written);

		try {
			$book->bookTitle = NULL;
			$this->fail();

		} catch (JetORM\Exception\InvalidArgumentException $e) {
			if ($e->getMessage() !== "Property 'Model\\Entities\\Book::\$bookTitle' cannot be NULL.") {
				throw $e;
			}
		}
	}

	public function testAnnotationFail()
	{
		try {
			$bad = new Model\Entities\BadEntity;
			$bad->toArray();
			$this->fail();

		} catch (JetORM\Exception\InvalidStateException $e) {
			if ($e->getMessage() !== 'Invalid property type (double NULL).') {
				throw $e;
			}
		}
	}

	public function testUninitializedColumnValue()
	{
		$book = ServiceLocator::getBookRepository()->createBook();
		$this->assertNull($book->toRow()->book_title);
	}

}
