<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/model/ServiceLocator.php';

\Model\ServiceLocator::getCacheStorage()->clean([
	Nette\Caching\Cache::ALL => TRUE,
]);

$loader = new Nette\Loaders\RobotLoader;
$loader->setCacheStorage(\Model\ServiceLocator::getCacheStorage());
$loader->addDirectory(__DIR__ . '/model');
$loader->register();

define('DB_HOST', '127.0.0.1');
define('DB_NAME', (getenv('MYSQL_DATABASE') !== FALSE) ? getenv('MYSQL_DATABASE') : 'jetorm_test');
define('DB_USER', (getenv('MYSQL_USER') !== FALSE) ? getenv('MYSQL_USER') : 'root');
define('DB_PASS', (getenv('MYSQL_PASSWORD') !== FALSE) ? getenv('MYSQL_PASSWORD') : '');
