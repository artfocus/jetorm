<?php

namespace Tests;

use Model;

class TestWithoutActiveRow extends \PHPUnit_Framework_TestCase
{

	/**
	 * @expectedException \Artfocus\JetORM\Exception\InvalidArgumentException
	 * @expectedExceptionMessage cannot be NULL
	 */
	public function testValues1()
	{
		$price = new Model\Entities\Price();
		$price->price;
	}

	public function testValues2()
	{
		$price = new Model\Entities\Price();
		$price->price = 15.0;
		$price->vat = 5.5;
		$price->total = 20.5;

		$this->assertSame(15.0, $price->price);
		$this->assertSame(5.5, $price->vat);
		$this->assertSame(20.5, $price->total);
	}

	public function testSerialization()
	{
		$price = new Model\Entities\Price();
		$price->price = 15.0;
		$price->vat = 5.5;
		$price->total = 20.5;

		$serialized = serialize($price);
		$unserialized = unserialize($serialized);

		$this->assertSame(15.0, $unserialized->price);
		$this->assertSame(5.5, $unserialized->vat);
		$this->assertSame(20.5, $unserialized->total);
	}

}
