
	ATTENTION:
	
	THIS PACKAGE IS NOT A THING YOU WANT TO WORK WITH.
	
	PLEASE DO NOT USE IT IF YOU HAVE AN OPTION.
	
	USE DOCTRINE 2 INSTEAD. REALLY. IT SAVES YOUR NERVES.


# Artfocus\JetORM

Lightweight ORM built on top of Nette\Database.
Based on [uestla/yetorm](https://github.com/uestla/YetORM), [hranicka/yetorm](https://bitbucket.org/hranicka/yetorm).
THANK YOU!


Quickstart
----------

Consider following database schema:

![Database schema](http://i.imgur.com/EtR1bM4.png)


### Installation

Setup `config.neon` like this:

	extensions:
		jetorm: Artfocus\JetORM\Extension
	
	yetorm:
		# setup cache IStorage for better performance
		# setup this only on production otherwise Entity doesn't load new Reflection until cache will be deleted!
		storage: cacheStorage # for development, leave this value empty (without "cacheStorage")

### Entities

Firstly we'll create entity classes according to the schema above.
There are two ways of defining entity properties - via `@property[-read]` annotation, or simply via getter and setter.

#### Tag

	/**
	 * @property-read int|null $id
	 * @property string $name
	 *
	 * @method int|null getId()
	 * @method string getName()
	 *
	 * @method Tag setName(string $name)
	 */
	class Tag extends Artfocus\JetORM\Entity
	{
		
	}


#### Author

	/**
	 * @property-read int|null $id
	 * @property string $name
	 * @property string $web
	 * @property \DateTime $born
	 *
	 * @method int|null getId()
	 * @method string getName()
	 * @method string getWeb()
	 * @method \DateTime getBorn()
	 *
	 * @method Author setName(string $name)
	 * @method Author setWeb(string $web)
	 * @method Author setBorn(\DateTime $born)
	 */
	class Author extends Artfocus\JetORM\Entity
	{
		
	}

#### Book

There are some relations at the `Book` entity - two N:1 `Author` and M:N `Tag` relations.
Every `Artfocus\JetORM\Entity` has an instance of `Artfocus\JetORM\Row` in it, which is a simple wrapper around
`Nette\Database\Table\ActiveRow`. That means that we can access related rows or column values through it.

	/**
	 * @property-read int|null $id
	 * @property string $title
	 * @property string $web
	 * @property string $slogan
	 * @property Author $author -> :one(author)
	 * @property Author $maintainer -> :one(author, maintainer_id)
	 * @property-read Tag[]|Artfocus\JetORM\Collection $tags -> :many(book_tag, tag)
	 *
	 * @method int|null getId()
	 * @method string getTitle()
	 * @method string getWeb()
	 * @method string getSlogan()
	 * @method Author getAuthor()
	 * @method Author getMaintainer()
	 * @method Tag[]|Artfocus\JetORM\Collection getTags()
	 *
	 * @method Book setTitle(string $title)
	 * @method Book setWeb(string $web)
	 * @method Book setSlogan(string $slogan)
	 * @method Book setAuthor(Author $author)
	 * @method Book setMaintainer(Author $maintainer)
	 */
	class Book extends Artfocus\JetORM\Entity
	{

	}


The M:N relation is realized with `Artfocus\JetORM\Collection` instance,
default `Artfocus\JetORM\EntityCollection` - which is a lazy collection of entities.

### Repositories

Every repository has to have table and entity class name defined - either via `@table` and `@entity` annotation, or via protected `$table` and `$entity` class property.

	/**
	 * @table book
	 * @entity Book
	 */
	class BookRepository extends Artfocus\JetORM\Repository
	{
		
	}


#### Persisting

To persist changes we make simply call `$repository->persist($entity)`.

	$book->setWeb('http://example.com');
	$books->persist($book);


And that's it!


Additional notes
----------------

- **No identity map**
- **Query efficiency** - the collections (resp. `Artfocus\JetORM\Row`) use the power of `Nette\Database` efficiency
- **Collection operations** - collections can be sorted via `$coll->orderBy($column, $dir)` and limited via `$coll->limit($limit, $offset)`


More
----

For more examples please see the [tests](https://bitbucket.org/artfocus/jetorm/src/master/tests/).
