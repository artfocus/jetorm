<?php

namespace Artfocus\JetORM;

class FieldOrder
{

	/** @var string */
	private $field;

	/** @var array */
	private $values;

	public function __construct($field, array $values)
	{
		$this->field = $field;
		$this->values = $values;
	}

	/**
	 * @return string
	 */
	public function getField()
	{
		return $this->field;
	}

	/**
	 * @return array
	 */
	public function getValues()
	{
		return $this->values;
	}

}
