<?php

namespace Artfocus\JetORM;

use Artfocus\JetORM;
use Nette;

class Row implements \ArrayAccess
{

	/** @var callable[] */
	public $onReload = [];

	/** @var Nette\Database\Table\ActiveRow|null */
	protected $row;

	/** @var array */
	protected $values = [];

	/** @var array */
	protected $modified = [];

	/** @param Nette\Database\Table\ActiveRow|array|null $row */
	public function __construct($row = NULL)
	{
		if (is_array($row)) {
			$this->modified = $row;
		} else {
			$this->row = $row;
		}
	}

	public function __clone()
	{
		if ($this->row) {
			$this->row->toArray(); // access all columns
			$this->row = clone $this->row;
		}
	}

	public function copy()
	{
		$clone = clone $this;

		// Copy values
		$native = $clone->getNative();
		$values = ($native) ? $native->toArray() : [];

		// Remove ActiveRow instance
		$clone->removeNative();

		// Fill values again
		foreach ($values as $key => $value) {
			$clone->__set($key, $value);
		}

		return $clone;
	}

	public function toArray()
	{
		$values = $this->modified + $this->values;
		if ($this->row) {
			$values += $this->row->toArray();
		}

		foreach ($values as & $value) {
			// run toArray also for loaded relations
			if ($value instanceof Nette\Database\Table\ActiveRow) {
				$value = $value->toArray();
			}

			// fixes serialize issue (invalid references?)
			if ($value instanceof \DateTime) {
				$value = clone $value;
			}
		}

		return $values;
	}

	/** @return bool */
	public function hasNative()
	{
		return $this->row !== NULL;
	}

	/** @return bool */
	public function isPersisted()
	{
		return $this->hasNative() && !count($this->modified);
	}

	/** @return Nette\Database\Table\ActiveRow|NULL */
	public function getNative()
	{
		return $this->row;
	}

	/**
	 * @param Nette\Database\Table\ActiveRow $row
	 * @return Row
	 */
	public function setNative(Nette\Database\Table\ActiveRow $row)
	{
		$this->reload($row);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function removeNative()
	{
		$this->row = NULL;
		return $this;
	}

	/**
	 * @param string $key
	 * @param string $throughColumn
	 * @return Nette\Database\Table\ActiveRow|NULL
	 */
	public function ref($key, $throughColumn = NULL)
	{
		$this->checkPersistence();
		return $this->row->ref($key, $throughColumn);
	}

	/**
	 * @param string $key
	 * @param string $throughColumn
	 * @return Nette\Database\Table\GroupedSelection
	 */
	public function related($key, $throughColumn = NULL)
	{
		$this->checkPersistence();
		return $this->row->related($key, $throughColumn);
	}

	/** @return array */
	public function getModified()
	{
		return $this->modified;
	}

	/** @return bool */
	public function update()
	{
		$this->checkPersistence();

		$status = TRUE;
		if (!$this->isPersisted()) {
			$status = $this->row->update($this->modified);
			$this->reload($this->row);
		}

		return $status;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function & __get($name)
	{
		if (array_key_exists($name, $this->modified)) {
			return $this->modified[$name];
		}

		if (array_key_exists($name, $this->values)) {
			return $this->values[$name];
		}

		if ($this->row === NULL) {
			return $this->row;
		}

		$value = $this->values[$name] = $this->row->$name;
		return $value;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->modified[$name] = $value;
	}

	public function get($name)
	{
		return $this->__get($name);
	}

	public function set($name, $value)
	{
		$this->__set($name, $value);
		return $this;
	}

	public function has($name)
	{
		return $this->__isset($name);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->modified[$name])
		|| isset($this->values[$name])
		|| isset($this->row->$name);
	}

	/**
	 * @throws Exception\InvalidStateException
	 */
	protected function checkPersistence()
	{
		if ($this->row === NULL) {
			throw new JetORM\Exception\RowNotSetException('Row not set yet.');
		}
	}

	/**
	 * @param Nette\Database\Table\ActiveRow $row
	 * @return void
	 */
	protected function reload(Nette\Database\Table\ActiveRow $row)
	{
		$this->row = $row;
		$this->modified = $this->values = [];

		foreach ($this->onReload as $callback) {
			$callback($this);
		}
	}

	public function offsetExists($offset)
	{
		return $this->__isset($offset);
	}

	public function offsetGet($offset)
	{
		return $this->__get($offset);
	}

	public function offsetSet($offset, $value)
	{
		return $this->__set($offset, $value);
	}

	public function offsetUnset($offset)
	{
		throw new JetORM\Exception\NotSupportedException();
	}

}
