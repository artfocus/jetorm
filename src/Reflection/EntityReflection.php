<?php

namespace Artfocus\JetORM\Reflection;

use Nette\Caching;

class EntityReflection
{

	const CACHE_KEY = 'reflections';

	/** @var EntityType[] */
	private static $reflections = [];

	/** @var bool */
	private static $hasChange = FALSE;

	/** @var bool */
	private static $hasShutdown = FALSE;

	/** @var Caching\Cache|null */
	private static $cache;

	public static function setCacheStorage(Caching\IStorage $storage = NULL)
	{
		if (!$storage) {
			self::$cache = NULL;
			return;
		}

		self::$cache = new Caching\Cache($storage, 'artfocus.jetorm');

		// Ignore invalid cache.
		try {
			$cachedReflection = (array)self::$cache->load(self::CACHE_KEY);
		} catch (\Throwable $e) {
			$cachedReflection = [];
		}

		self::$reflections = array_merge(
			$cachedReflection,
			self::$reflections
		);

		if (!self::$hasShutdown) {
			self::$hasShutdown = TRUE;
			register_shutdown_function([__CLASS__, 'save']);
		}
	}

	/**
	 * @param string $class
	 * @return EntityType
	 */
	public static function getReflection($class)
	{
		if (!isset(self::$reflections[$class])) {
			self::$reflections[$class] = new EntityType($class);
			self::$hasChange = TRUE;
		}

		return self::$reflections[$class];
	}

	/**
	 * @internal
	 */
	public static function save()
	{
		if (self::$hasChange && self::$cache) {
			self::$cache->save('reflections', self::$reflections);
		}
	}

}
