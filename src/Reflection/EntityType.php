<?php

namespace Artfocus\JetORM\Reflection;

use Artfocus\JetORM;
use Nette\Reflection as NReflection;
use Nette\Utils\Strings as NStrings;

class EntityType implements \Serializable
{

	/** @var string */
	private $class;

	/** @var string */
	private $name;

	/** @var EntityProperty[] */
	private $entityProperties;

	/** @var AnnotationProperty[] */
	private $annotationProperties = [];

	/** @var NReflection\ClassType */
	private $reflection;

	public function __construct($class)
	{
		$this->class = $class;
	}

	public function serialize()
	{
		$this->loadEntityProperties();

		return serialize([
			'class' => $this->class,
			'name' => $this->getName(),
			'entityProperties' => $this->entityProperties,
			'annotationProperties' => $this->annotationProperties,
		]);
	}

	public function unserialize($serialized)
	{
		$unserialized = @unserialize($serialized);
		if ($unserialized === FALSE) {
			throw new JetORM\Exception\InvalidStateException;
		}

		$this->class = $unserialized['class'];
		$this->name = $unserialized['name'];
		$this->entityProperties = $unserialized['entityProperties'];
		$this->annotationProperties = $unserialized['annotationProperties'];
	}

	/**
	 * @return NReflection\ClassType
	 */
	public function getReflection()
	{
		if (!$this->reflection) {
			$this->reflection = NReflection\ClassType::from($this->class);
		}

		return $this->reflection;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		if ($this->name === NULL) {
			$this->name = $this->getReflection()->getName();
		}

		return $this->name;
	}

	/**
	 * @return EntityProperty[]
	 */
	public function getEntityProperties()
	{
		$this->loadEntityProperties();
		return $this->entityProperties;
	}

	/**
	 * @param string $name
	 * @param null $default
	 * @return EntityProperty|AnnotationProperty|null
	 */
	public function getEntityProperty($name, $default = NULL)
	{
		return $this->hasEntityProperty($name) ? $this->entityProperties[$name] : $default;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function hasEntityProperty($name)
	{
		$this->loadEntityProperties();
		return isset($this->entityProperties[$name]);
	}

	/**
	 * @return array
	 */
	private function getClassTree()
	{
		$tree = [];
		$current = $this->getName();

		do {
			$tree[] = $current;
			$current = get_parent_class($current);

		} while ($current !== FALSE && $current !== 'Artfocus\\JetORM\\Entity');

		return array_reverse($tree);
	}

	private function loadEntityProperties()
	{
		if ($this->entityProperties === NULL) {
			$this->entityProperties = [];

			foreach ($this->getClassTree() as $class) {
				$this->loadAnnotationProperties($class);

				foreach ($this->annotationProperties[$class] as $name => $property) {
					$this->entityProperties[$name] = $property;
				}
			}

			$this->loadMethodProperties();
		}
	}

	private function loadMethodProperties()
	{
		foreach ($this->getReflection()->getMethods(NReflection\Method::IS_PUBLIC) as $method) {
			$name = lcfirst(substr($method->name, 3));

			if (!isset($this->entityProperties[$name])
				&& $method->declaringClass->name !== 'Artfocus\\JetORM\\Entity'
				&& strlen($method->name) > 3 && substr($method->name, 0, 3) === 'get'
				&& !$method->hasAnnotation('internal')
				&& !$method->hasAnnotation('no-serialize')
			) {
				$type = $method->getAnnotation('return');

				// support NULL type
				$nullableType = $this->getNullableType($type);
				$nullable = !!$nullableType;

				if ($nullableType) {
					$type = $nullableType;
				}

				if (!EntityProperty::isNativeType($type)) {
					$type = NReflection\AnnotationsParser::expandClassName($type, $this->getReflection());
				}

				$this->entityProperties[$name] = new MethodProperty(
					$this,
					$name,
					!$this->getReflection()->hasMethod('set' . ucfirst($name)),
					$type,
					$nullable
				);
			}
		}
	}

	/**
	 * @param string $type
	 * @return string|false
	 */
	private function getNullableType($type)
	{
		// support NULL type
		$nullable = FALSE;
		$types = explode('|', $type, 2);
		if (count($types) === 2) {
			if (strcasecmp($types[0], 'null') === 0) {
				$type = $types[1];
				$nullable = TRUE;
			}

			if (strcasecmp($types[1], 'null') === 0) {
				if ($nullable) {
					throw new JetORM\Exception\InvalidStateException('Invalid property type (double NULL).');
				}

				$type = $types[0];
				$nullable = TRUE;
			}
		}

		if ($nullable) {
			return $type;
		}

		return FALSE;
	}

	/**
	 * @param string $class
	 */
	private function loadAnnotationProperties($class)
	{
		if (isset($this->annotationProperties[$class])) {
			return;
		}

		$this->annotationProperties[$class] = [];

		/** @var EntityType $ref */
		$ref = $class::getEntityType();

		foreach ($ref->getReflection()->getAnnotations() as $ann => $values) {
			if ($ann !== 'property' && $ann !== 'property-read') {
				continue;
			}

			foreach ($values as $tmp) {
				$split = NStrings::split($tmp, '#\s#');

				if (count($split) >= 2) {
					list($type, $var) = $split;

					// support NULL type
					$nullableType = $this->getNullableType($type);
					$nullable = !!$nullableType;

					if ($nullableType) {
						$type = $nullableType;
					}

					// unify type name
					if ($type === 'bool') {
						$type = 'boolean';

					} elseif ($type === 'int') {
						$type = 'integer';
					}

					if (!EntityProperty::isNativeType($type)) {
						$type = NReflection\AnnotationsParser::expandClassName($type, $ref->getReflection());
					}

					$name = substr($var, 1);
					$readonly = $ann === 'property-read';

					// parse column name
					$column = $name;
					if (isset($split[2]) && $split[2] === '->' && isset($split[3])) {
						$column = $split[3];
					}

					$this->annotationProperties[$class][$name] = new AnnotationProperty(
						$ref,
						$name,
						$readonly,
						$type,
						$nullable,
						$column
					);
				}
			}
		}
	}

}
