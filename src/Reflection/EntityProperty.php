<?php

namespace Artfocus\JetORM\Reflection;

use Artfocus\JetORM;

abstract class EntityProperty
{

	/** @var EntityType */
	private $reflection;

	/** @var string */
	private $name;

	/** @var bool */
	private $readOnly;

	/** @var string */
	private $type;

	/** @var bool */
	private $nullable;

	/**
	 * @param EntityType $reflection
	 * @param string $name
	 * @param bool $readOnly
	 * @param string $type
	 * @param bool $nullable
	 */
	public function __construct(EntityType $reflection, $name, $readOnly, $type, $nullable)
	{
		$this->reflection = $reflection;
		$this->name = (string)$name;
		$this->readOnly = (bool)$readOnly;
		$this->type = $this->getCommonType($type);
		$this->nullable = (bool)$nullable;
	}

	/**
	 * @return EntityType
	 */
	public function getEntityReflection()
	{
		return $this->reflection;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isReadOnly()
	{
		return $this->readOnly;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return bool
	 */
	public function isNullable()
	{
		return $this->nullable;
	}

	/**
	 * @param mixed $value
	 * @return mixed
	 */
	public function setType($value)
	{
		if (!$this->checkType($value, FALSE) && @settype($value, $this->getType()) === FALSE) { // intentionally @
			throw new JetORM\Exception\InvalidArgumentException("Unable to set type '{$this->getType()}' from '"
				. gettype($value) . "'.");
		}

		return $value;
	}

	/**
	 * @param mixed $value
	 * @param bool $need
	 * @return mixed
	 */
	public function checkType($value, $need = TRUE)
	{
		if ($value === NULL) {
			if (!$this->nullable) {
				$entity = $this->getEntityReflection()->getName();
				throw new JetORM\Exception\AnnotationException("Property '{$entity}::\${$this->getName()}' cannot be NULL.");
			}

		} elseif (!$this->isOfNativeType()) {
			$class = $this->getType();
			if (!($value instanceof $class)) {
				throw new JetORM\Exception\AnnotationException("Instance of '{$class}' expected, '"
					. get_class($value) . "' given.");
			}

		} elseif ($need && ($type = gettype($value)) !== $this->getType()) {
			throw new JetORM\Exception\AnnotationException("Invalid type - '{$this->getType()}' expected, '$type' given.");

		} else {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * @return bool
	 */
	public function isOfNativeType()
	{
		return self::isNativeType($this->type);
	}

	/**
	 * @param string $type
	 * @return bool
	 */
	public static function isNativeType($type)
	{
		return ($type === NULL || $type === 'integer' || $type === 'float' || $type === 'double'
			|| $type === 'boolean' || $type === 'string' || $type === 'array');
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function getCommonType($value)
	{
		switch ($value) {
			case 'double':
			case 'float':
			case 'real':
				return 'double';
			case 'boolean':
			case 'bool':
				return 'boolean';
			case 'integer':
			case 'int':
				return 'integer';
			default:
				return (string)$value;
		}
	}

}
