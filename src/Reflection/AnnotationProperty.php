<?php

namespace Artfocus\JetORM\Reflection;

use Artfocus\JetORM;

class AnnotationProperty extends EntityProperty
{

	/** @var string */
	private $column;

	/**
	 * @param EntityType $reflection
	 * @param string $name
	 * @param bool $readonly
	 * @param string $type
	 * @param bool $nullable
	 * @param string $column
	 */
	public function __construct($reflection, $name, $readonly, $type, $nullable, $column)
	{
		parent::__construct($reflection, $name, $readonly, $type, $nullable);
		$this->column = (string)$column;
	}

	/**
	 * @return string
	 */
	public function getColumn()
	{
		return $this->column;
	}

	/**
	 * @return array|false
	 */
	public function getOneToOne()
	{
		if (preg_match('~:one\(([a-zA-Z_]+)(?:,\s*([a-zA-Z_]+))?\)~', $this->getColumn(), $m)) {
			return [
				'entity' => $this->getSingleType(),
				'relTable' => $m[1],
				'throughColumn' => (isset($m[2])) ? $m[2] : NULL,
				'required' => !$this->isNullable(),
			];
		}

		return FALSE;
	}

	/**
	 * @return array|false
	 */
	public function getManyToOne()
	{
		if (preg_match('~:many\(([a-zA-Z_]+)(?:,\s*([a-zA-Z_]+))?(?:,\s*([a-zA-Z_]+))?\)~', $this->getColumn(), $m)) {
			return [
				'entity' => $this->getSingleType(),
				'relTable' => $m[1],
				'entityTable' => (isset($m[2])) ? $m[2] : NULL,
				'throughColumn' => (isset($m[3])) ? $m[3] : NULL,
			];
		}

		return FALSE;
	}

	/**
	 * @return string
	 */
	private function getSingleType()
	{
		if (preg_match('~^([a-zA-Z0-9_\\\]+)~', $this->getType(), $m)) {
			return $m[1];
		}

		throw new JetORM\Exception\InvalidArgumentException("Cannot parse single class name from '{$this->getType()}'.");
	}

}
