<?php

namespace Artfocus\JetORM;

class EntityReference
{

	/** @var int */
	private $class;

	/** @var string */
	private $hash;

	public function __construct(Entity $entity)
	{
		$this->class = get_class($entity);
		$this->hash = $this->class . ':' . md5($this->stringify($entity->toArrayRecursive(1)));
	}

	/**
	 * @return int
	 */
	public function getClass()
	{
		return $this->class;
	}

	/**
	 * @return string
	 */
	public function getHash()
	{
		return $this->hash;
	}

	/**
	 * @param array $array
	 * @return string
	 */
	private function stringify(array $array)
	{
		$return = '';
		foreach ($array as $key => $value) {
			$stringValue = (is_array($value)) ?
				$this->stringify($value) :
				(string)$value;

			$return .= ";$key=" . $stringValue;
		}

		return $return;
	}

}
