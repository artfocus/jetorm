<?php

namespace Artfocus\JetORM;

interface Collection extends \Iterator, \Countable
{

	const ASC = FALSE;
	const DESC = TRUE;

	/**
	 * @return array
	 */
	public function toArray();

	/**
	 * @return Collection
	 */
	public function resetOrder();

	/**
	 * @param string|array $column
	 * @param bool $direction
	 * @return Collection
	 */
	public function orderBy($column, $direction = NULL);

	/**
	 * @param int $limit
	 * @param int $offset
	 * @return Collection
	 */
	public function limit($limit, $offset = NULL);

	/**
	 * @param mixed $column
	 * @return int
	 */
	public function countDistinct($column);

	/**
	 * @param string $function
	 * @return mixed
	 */
	public function aggregation($function);

}
