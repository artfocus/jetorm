<?php

namespace Artfocus\JetORM;

use Artfocus\JetORM\Exception;
use Artfocus\JetORM\Reflection;
use Nette;

/**
 * @property-read int|null $id
 *
 * @method int|null getId()
 */
abstract class Entity implements \IteratorAggregate, \ArrayAccess, \Serializable
{

	/** @var callable[] */
	public $onPersist = [];

	/** @var callable[] */
	public $onDelete = [];

	/** @var callable[] */
	public $onRowChange = [];

	/** @var Row */
	protected $row;

	/** @var array */
	private $values = [];

	/** @var array */
	private $references = [];

	/** @var EntitySleeper */
	private $sleeper;

	/**
	 * @return Reflection\EntityType
	 */
	public static function getEntityType()
	{
		return Reflection\EntityReflection::getReflection(get_called_class());
	}

	/**
	 * @return Nette\Reflection\ClassType
	 */
	public static function getReflection()
	{
		return self::getEntityType()->getReflection();
	}

	/**
	 * It's useful for ::getOne, ::getMany, etc.
	 * @return string
	 * @deprecated Use PHP 5.5's ::class, man!
	 */
	public static function getClassName()
	{
		return get_called_class();
	}

	/**
	 * @param Nette\Database\Table\ActiveRow|array|null|Row $data
	 */
	public function __construct($data = NULL)
	{
		$this->createRow($data);
		if (!$this->getRow()->hasNative()) {
			$this->init();
		}
	}

	public function __clone()
	{
		$row = clone $this->getRow();
		$this->fromRow($row);
	}

	/**
	 * Resets specified column, by default ID.
	 * It's useful when saving cloned Entity with unique key.
	 * @param string $column
	 * @param int $depth
	 * @return $this
	 */
	public function reset($column = 'id', $depth = 99)
	{
		$this->getRow()->$column = NULL;

		if ($depth--) {
			foreach ($this->toArray() as $key => $value) {
				if ($value instanceof Entity) {
					$value->reset($column, $depth);
					$this->$key = $value;
				}
			}
		}

		return $this;
	}

	/**
	 * Returns Entity clone without ActiveRow reference inside.
	 * @return Entity
	 */
	public function copy()
	{
		$clone = clone $this;
		$clone->getEntityValues();
		$clone->fromRow($clone->toRow()->copy());
		return $clone;
	}

	/**
	 * @param string $name
	 * @param array $args
	 * @return mixed
	 * @throws Exception\MemberAccessException
	 */
	public function __call($name, $args)
	{
		$getter = $this->nameOfGetter($name);
		if (static::getEntityType()->getEntityProperty($getter)) {
			$value = $this->__get($getter);
			return $value;
		}

		$setter = $this->nameOfSetter($name);
		if (static::getEntityType()->getEntityProperty($setter)) {
			$value = $this->__set($setter, array_shift($args));
			return $value;
		}

		$class = get_class($this);
		throw new Exception\MemberAccessException("Call to undefined method $class::$name().");
	}

	/**
	 * @param string $name
	 * @return mixed
	 * @throws Exception\MemberAccessException
	 */
	public function & __get($name)
	{
		$getter = $this->formatGetter($name);
		$prop = static::getEntityType()->getEntityProperty($name);
		$methodExists = $this->hasMethod($getter);

		if (($prop && $methodExists) || ($methodExists)) {
			$value = $this->callMethod($getter);
			return $value;

		} elseif ($prop) {
			if ($prop instanceof Reflection\MethodProperty) {
				return $this->getRow()->{$prop->getName()};

			} elseif ($prop instanceof Reflection\AnnotationProperty) {
				if (($args = $prop->getOneToOne())) {
					$value = $this->getOne($args['entity'], $args['relTable'], $args['throughColumn'], $args['required'], $name);
					return $value;

				} elseif (($args = $prop->getManyToOne())) {
					$value = $this->getMany($args['entity'], $args['relTable'], $args['entityTable'], $args['throughColumn'], [], $name);
					return $value;
				}

				$value = $prop->setType($this->getRow()->{$prop->getColumn()});
				return $value;
			}
		}

		$class = get_class($this);
		throw new Exception\MemberAccessException("Cannot read an undeclared property $class::\$$name.");
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 * @throws Exception\MemberAccessException
	 */
	public function __set($name, $value)
	{
		$setter = $this->formatSetter($name);
		$prop = static::getEntityType()->getEntityProperty($name);

		if ($prop && $this->hasMethod($setter)) {
			$this->callMethod($setter, [$value]);

		} elseif ($prop && !$prop->isReadOnly()) {
			if ($prop instanceof Reflection\MethodProperty) {
				$this->getRow()->{$prop->getName()} = $value;

			} elseif ($prop instanceof Reflection\AnnotationProperty) {
				if ($prop instanceof Reflection\AnnotationProperty) {
					if (($args = $prop->getOneToOne())) {
						return $this->setOne($args['relTable'], $value, $name);
					}
				}

				$prop->checkType($value);
				$this->getRow()->{$prop->getColumn()} = $value;
			}

		} else {
			$class = get_class($this);
			throw new Exception\MemberAccessException("Cannot write to an undeclared property $class::\$$name.");
		}

		return $this;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		$prop = static::getEntityType()->getEntityProperty($name);
		if ($prop && $this->hasMethod($this->formatGetter($name))) {
			$value = $this->callMethod($this->formatGetter($name));
			return $value !== NULL;

		} elseif ($prop) {
			return $this->__get($name) !== NULL;
		}

		return FALSE;
	}

	/**
	 * @param string $name
	 * @return void
	 * @throws Exception\NotSupportedException
	 */
	public function __unset($name)
	{
		throw new Exception\NotSupportedException;
	}

	public function serialize()
	{
		$this->sleeper = new EntitySleeper($this);
		return serialize($this->sleeper);
	}

	public function unserialize($serialized)
	{
		$sleeper = unserialize($serialized);
		$this->wakeUp($sleeper);
	}

	/**
	 * @param EntitySleeper $sleeper
	 * @param EntityReference|null $reference
	 * @internal
	 */
	public function wakeUp(EntitySleeper $sleeper, EntityReference $reference = NULL)
	{
		$this->sleeper = $sleeper;
		$values = $sleeper->getValues($reference);

		$this->values = $values['values'];
		$this->references = [];

		$this->createRow($values['row']);
	}

	/**
	 * @return Row
	 */
	public function toRow()
	{
		$this->refreshEntityReferences();
		return $this->getRow();
	}

	public function fromRow(Row $row)
	{
		$this->row = $row;

		$row->onReload[] = function () {
			$this->triggerOnRowChange();
		};

		$this->triggerOnRowChange();

		return $this;
	}

	/**
	 * Looks for all public get* methods and @property[-read] annotations
	 * and returns associative array with corresponding values
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->getEntityValues(TRUE);
	}

	/**
	 * @param bool $exceptions
	 * @return array
	 * @throws \Exception
	 * @deprecated
	 */
	public function loadReferences($exceptions = FALSE)
	{
		return $this->getEntityValues($exceptions);
	}

	/**
	 * @param bool $exceptions
	 * @return array
	 * @throws \Exception
	 */
	public function getEntityValues($exceptions = FALSE)
	{
		$data = [];

		$ref = static::getEntityType();
		foreach ($ref->getEntityProperties() as $name => $property) {
			try {
				if ($property instanceof Reflection\MethodProperty) {
					$value = $this->{'get' . $name}();
				} else {
					$value = $this->$name;
				}
			} catch (\Exception $e) {
				if ($exceptions) {
					throw $e;
				} else {
					continue;
				}
			}

			// expand and check collection
			if ($value instanceof Collection) {
				/** @var Collection|Entity[] $collection */
				$collection = $value;
				$value = [];

				foreach ($collection as $entity) {
					$value[] = $entity;
				}
			}

			$data[$name] = $value;
		}

		return $data;
	}

	/**
	 * @return $this
	 */
	public function refresh()
	{
		$this->values = [];
		$this->references = [];
		return $this;
	}

	public function getIterator()
	{
		return new \ArrayIterator($this->toArray());
	}

	public function offsetExists($offset)
	{
		try {
			$this->__get($offset);
			return TRUE;
		} catch (Exception\MemberAccessException $e) {
			return FALSE;
		}
	}

	public function offsetGet($offset)
	{
		return $this->__get($offset);
	}

	public function offsetSet($offset, $value)
	{
		$this->__set($offset, $value);
	}

	public function offsetUnset($offset)
	{
		$this->__unset($offset);
	}

	/**
	 * @param int|null $maxDepth Max. depth to which Entities are expanded.
	 * @return array
	 */
	public function toArrayRecursive($maxDepth = NULL)
	{
		return $this->expandEntity($this->toArray(), [$this], $maxDepth);
	}

	/**
	 * @param array $data
	 * @param bool $exceptions
	 * @return $this
	 * @throws Exception\InvalidArgumentException
	 */
	public function fromArray($data, $exceptions = FALSE)
	{
		// Set objects first (relations, Row not set errors preventing).
		uasort($data, function ($a, $b) {
			if (is_object($a) && is_object($b)) {
				return 0;
			}

			return is_object($a) ? -1 : 1;
		});

		foreach ($data as $setter => $value) {
			if (PHP_VERSION_ID >= 70000) {
				try {
					$this->__set($setter, $value);
				} catch (\Throwable $e) {
					if ($exceptions) {
						throw new Exception\InvalidArgumentException("Attribute '$setter' is not writable.");
					}
				}
			} else {
				try {
					$this->__set($setter, $value);
				} catch (\Exception $e) {
					if ($exceptions) {
						throw new Exception\InvalidArgumentException("Attribute '$setter' is not writable.");
					}
				}
			}
		}

		return $this;
	}

	/**
	 * Method is called only when Row is empty
	 * (new empty entity was created)
	 */
	protected function init()
	{
	}

	/**
	 * @return Row
	 */
	protected function getRow()
	{
		if (!$this->row) {
			$this->createRow();
		}

		return $this->row;
	}

	/**
	 * @param Row $row
	 * @return $this
	 * @deprecated Use ::fromRow instead.
	 */
	protected function setRow(Row $row)
	{
		$this->fromRow($row);
		return $this;
	}

	protected function hasMethod($name)
	{
		return method_exists($this, $name);
	}

	protected function callMethod($name, array $attrs = [])
	{
		return call_user_func_array([$this, $name], $attrs);
	}

	/**
	 * @param string $column
	 * @param Entity|null $entity
	 * @param string|null $key
	 * @return $this
	 */
	protected function setOne($column, Entity $entity = NULL, $key = NULL)
	{
		$key = ($key) ?: $this->nameOfSetter($this->getCallerMethod());
		$this->setEntityValue($entity, $key);
		$this->setEntityReference($column, $entity);

		return $this;
	}

	/**
	 * @param mixed $value
	 * @param string|null $key
	 * @return $this
	 */
	protected function setEntityValue($value, $key = NULL)
	{
		$key = ($key) ?: $this->nameOfSetter($this->getCallerMethod());
		$this->values[$key] = $value;

		return $this;
	}

	/**
	 * @param string|callable $entity
	 * @param string $relTable
	 * @param string $entityTable
	 * @param string $throughColumn
	 * @param array $criteria
	 * @param string|null $key
	 * @return Collection
	 */
	protected function getMany($entity, $relTable, $entityTable = NULL, $throughColumn = NULL, $criteria = [], $key = NULL)
	{
		$key = ($key) ?: $this->nameOfGetter($this->getCallerMethod());
		return $this->getEntityValue($key, function () use ($relTable, $entity, $entityTable, $throughColumn, $criteria) {
			// Don't fall eg. for serialized entity which has been extended with new method.
			if (!$this->getRow()->hasNative()) {
				return new EntityCollection();
			}

			$selection = $this->getRow()->related($relTable);
			if ($criteria) {
				$selection->where($criteria);
			}

			return new EntityCollection($selection, $entity, $entityTable, $throughColumn);
		});
	}

	/**
	 * @param string $entity
	 * @param string $relTable
	 * @param string|null $throughColumn
	 * @param bool $required
	 * @param string|null $key
	 * @return Entity|null
	 */
	protected function getOne($entity, $relTable, $throughColumn = NULL, $required = FALSE, $key = NULL)
	{
		$fallback = function () use ($entity, $relTable, $throughColumn, $required) {
			if ($required || $this->getRow()->hasNative()) {
				$row = $this->getRow()->ref($relTable, $throughColumn);
			} else {
				$row = NULL;
			}

			if ($row) {
				return new $entity($row);
			} elseif ($required) {
				throw new Exception\InvalidStateException("Reference to '$relTable' was not found.");
			}

			return NULL;
		};

		$key = ($key) ?: $this->nameOfGetter($this->getCallerMethod());
		$object = $this->getEntityValue($key, $fallback);

		// Refresh entity reference
		if ($object && $throughColumn) {
			$this->setEntityReference($throughColumn, $object);
		}

		return $object;
	}

	/**
	 * @param string|null $key
	 * @param callable|null $fallback
	 * @return mixed
	 */
	protected function getEntityValue($key = NULL, callable $fallback = NULL)
	{
		$key = ($key) ?: $this->nameOfGetter($this->getCallerMethod());

		if (array_key_exists($key, $this->values)) {
			$value = $this->values[$key];
			return $this->wakeUpValue($value);
		}

		$value = ($fallback) ? call_user_func($fallback) : NULL;
		$this->setEntityValue($value, $key);
		return $value;
	}

	private function refreshEntityReferences()
	{
		foreach ($this->references as $column => $entity) {
			$this->setEntityReference($column, $entity);
		}
	}

	/**
	 * @param string $column
	 * @param Entity $entity
	 * @return $this
	 */
	private function setEntityReference($column, Entity $entity = NULL)
	{
		if (!Nette\Utils\Strings::endsWith($column, '_id')) {
			$column .= '_id';
		}

		$this->getRow()->$column = ($entity) ? $entity->getId() : NULL;
		$this->references[$column] = $entity;

		return $this;
	}

	/**
	 * @param array $values
	 * @param Entity[] $ignored
	 * @param int|null $maxDepth
	 * @param int $depth
	 * @return array
	 */
	private function expandEntity(array $values, array $ignored = [], $maxDepth = NULL, $depth = 0)
	{
		foreach ($values as $key => & $value) {
			// check ignored objects (infinite recursion loop workaround)
			foreach ($ignored as $ignore) {
				if ($value instanceof $ignore && $value->getId() === $ignore->getId()) {
					unset($values[$key]);
					continue 2;
				}
			}

			// expand Entity if $maxDepth has not been reached yet
			if ($value instanceof Entity) {
				if ($maxDepth !== NULL && $depth > $maxDepth) {
					unset($values[$key]);
					continue;
				}

				$value = $this->expandEntity($value->toArray(), array_merge($ignored, [$value]), $maxDepth, $depth + 1);
			}

			// check for collections and expand them if $maxDepth has not been reached yet
			if (is_array($value) && $value) {
				$value = $this->expandEntity($value, $ignored, $maxDepth, $depth + 1);
			}
		}

		return $values;
	}

	/**
	 * @param mixed $value
	 * @return Entity
	 */
	private function wakeUpValue($value)
	{
		if ($this->sleeper) {
			if ($value instanceof EntityReference) {
				$value = $this->sleeper->wakeUp($value);
			}

			if (is_array($value)) {
				foreach ($value as & $subValue) {
					if ($subValue instanceof EntityReference) {
						$subValue = $this->sleeper->wakeUp($subValue);
					}
				}
			}
		}

		return $value;
	}

	/**
	 * IMPORTANT! Call only directly due to debug_backtrace implementation!
	 * @return string
	 */
	private function getCallerMethod()
	{
		return debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3)[2]['function'];
	}

	/**
	 * @param Nette\Database\Table\ActiveRow|array|null|Row $data
	 * @return $this
	 */
	private function createRow($data = NULL)
	{
		$row = ($data instanceof Row) ? $data : new Row($data);
		$this->fromRow($row);
		return $this;
	}

	private function triggerOnRowChange()
	{
		foreach ($this->onRowChange as $callback) {
			$callback($this);
		}
	}

	private function formatGetter($name)
	{
		return 'get' . ucfirst($name);
	}

	private function formatSetter($name)
	{
		return 'set' . ucfirst($name);
	}

	private function nameOfGetter($name)
	{
		if (Nette\Utils\Strings::startsWith($name, 'get')) {
			return lcfirst(Nette\Utils\Strings::substring($name, 3));
		}

		return NULL;
	}

	private function nameOfSetter($name)
	{
		if (Nette\Utils\Strings::startsWith($name, 'set')) {
			return lcfirst(Nette\Utils\Strings::substring($name, 3));
		}

		return NULL;
	}

}
