<?php

namespace Artfocus\JetORM;

class CriteriaQuery implements Query
{

	/** @var array */
	private $criteria;

	/**
	 * @param array $criteria
	 */
	public function __construct(array $criteria)
	{
		$this->criteria = $criteria;
	}

	/**
	 * @param Repository $repository
	 * @return Collection|Entity[]
	 */
	public function run(Repository $repository)
	{
		return $repository->findBy($this->criteria);
	}

}
