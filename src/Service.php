<?php

namespace Artfocus\JetORM;

abstract class Service
{

	/** @var Repository */
	protected $repository;

	public function __construct(Repository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @return Entity
	 */
	public function create()
	{
		return $this->repository->create();
	}

	/**
	 * @param int $id
	 * @return null|Entity
	 */
	public function find($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * @return Collection|Entity[]
	 */
	public function findAll()
	{
		return $this->repository->findAll();
	}

	/**
	 * @param Entity $entity
	 * @return bool
	 */
	public function persist(Entity $entity)
	{
		return $this->repository->persist($entity);
	}

	/**
	 * @param Entity $entity
	 * @return bool
	 */
	public function delete(Entity $entity)
	{
		return $this->repository->delete($entity);
	}

	/**
	 * @param Query $query
	 * @return Collection|Entity[]
	 */
	public function findByQuery(Query $query)
	{
		return $query->run($this->repository);
	}

	/**
	 * @param \Closure $callback
	 * @return mixed
	 * @throws \Exception
	 */
	protected function transaction(\Closure $callback)
	{
		return $this->repository->transaction($callback);
	}

}
