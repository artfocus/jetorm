<?php

namespace Artfocus\JetORM;

use Nette;

class Extension extends Nette\DI\CompilerExtension
{

	/** @var array */
	private $defaults = [
		'storage' => NULL,
	];

	public function afterCompile(Nette\PhpGenerator\ClassType $class)
	{
		$initialize = $class->methods['initialize'];
		$config = $this->getConfig($this->defaults);

		if ($config['storage']) {
			$initialize->addBody('\Artfocus\JetORM\Reflection\EntityReflection::setCacheStorage($this->getService(?));', [
				$config['storage'],
			]);
		}
	}

}
