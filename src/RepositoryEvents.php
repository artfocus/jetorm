<?php

namespace Artfocus\JetORM;

use Nette;

class RepositoryEvents extends Nette\Object
{

	const CREATE = 'create';
	const BEFORE_PERSIST = 'beforePersist';
	const AFTER_PERSIST = 'afterPersist';
	const BEFORE_PERSIST_NEW = 'beforePersistNew';
	const AFTER_PERSIST_NEW = 'afterPersistNew';
	const BEFORE_DELETE = 'beforeDelete';
	const AFTER_DELETE = 'afterDelete';

	/**
	 * @var callable[]
	 */
	public $onCreate = [];

	/**
	 * @var callable[]
	 */
	public $onBeforePersist = [];

	/**
	 * @var callable[]
	 */
	public $onAfterPersist = [];

	/**
	 * @var callable[]
	 */
	public $onBeforePersistNew = [];

	/**
	 * @var callable[]
	 */
	public $onAfterPersistNew = [];

	/**
	 * @var callable[]
	 */
	public $onBeforeDelete = [];

	/**
	 * @var callable[]
	 */
	public $onAfterDelete = [];

	/**
	 * @param RepositoryEvents $events
	 * @return $this
	 */
	public function composeEvents(RepositoryEvents $events)
	{
		$this->bindComposeEvents($events, self::CREATE);
		$this->bindComposeEvents($events, self::BEFORE_PERSIST);
		$this->bindComposeEvents($events, self::AFTER_PERSIST);
		$this->bindComposeEvents($events, self::BEFORE_PERSIST_NEW);
		$this->bindComposeEvents($events, self::AFTER_PERSIST_NEW);
		$this->bindComposeEvents($events, self::BEFORE_DELETE);
		$this->bindComposeEvents($events, self::AFTER_DELETE);
		return $this;
	}

	/**
	 * @param string $event
	 * @param callable $callback
	 * @return $this
	 */
	public function handle($event, callable $callback)
	{
		$this->getEventHandlers($event)[] = $callback;
		return $this;
	}

	/**
	 * @param string $event
	 * @param array $parameters
	 * @return $this
	 */
	public function invoke($event, array $parameters)
	{
		return $this->invokeHandlers($this->getEventHandlers($event), $parameters);
	}

	/**
	 * @param callable[] $handlers
	 * @param array $parameters
	 * @return $this
	 */
	public function invokeHandlers($handlers, array $parameters)
	{
		foreach ($handlers as $callback) {
			call_user_func_array($callback, $parameters);
		}

		return $this;
	}

	/**
	 * @param string $event
	 * @return array
	 */
	private function & getEventHandlers($event)
	{
		$map = [
			self::CREATE => & $this->onCreate,
			self::BEFORE_PERSIST => & $this->onBeforePersist,
			self::AFTER_PERSIST => & $this->onAfterPersist,
			self::BEFORE_PERSIST_NEW => & $this->onBeforePersistNew,
			self::AFTER_PERSIST_NEW => & $this->onAfterPersistNew,
			self::BEFORE_DELETE => & $this->onBeforeDelete,
			self::AFTER_DELETE => & $this->onAfterDelete,
		];

		if (isset($map[$event])) {
			return $map[$event];
		}

		throw new Exception\InvalidArgumentException("Event '$event' does not exist.");
	}

	private function bindComposeEvents(RepositoryEvents $events, $event)
	{
		$this->handle($event, function (Entity $entity, Repository $repository) use ($event, $events) {
			$events->invoke($event, [$entity, $repository]);
		});
	}

}
