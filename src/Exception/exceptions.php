<?php

namespace Artfocus\JetORM\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{

}

class InvalidStateException extends \RuntimeException
{

}

class MemberAccessException extends \LogicException
{

}

class NotSupportedException extends \LogicException
{

}

class RowNotSetException extends InvalidStateException
{

}

class AnnotationException extends InvalidArgumentException
{

}
