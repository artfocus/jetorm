<?php

namespace Artfocus\JetORM;

use Artfocus\JetORM\Exception;
use Nette;

/**
 * @property-read Nette\Database\Table\Selection $table
 */
abstract class Repository extends RepositoryEvents
{

	/** @var array */
	public static $transactionCounter = [];

	/** @var array */
	private static $reflections = [];

	/** @var Nette\Database\Context */
	protected $database;

	/** @var string */
	protected $tableName = NULL;

	/** @var string */
	protected $entity = NULL;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;

		if (!isset(self::$transactionCounter[$dsn = $database->getConnection()->getDsn()])) {
			self::$transactionCounter[$dsn] = 0;
		}
	}

	/**
	 * @param Nette\Database\Table\ActiveRow|array|null $row
	 * @return Entity
	 */
	public function create(Nette\Database\Table\ActiveRow $row = NULL)
	{
		$class = $this->getEntityClass();
		$entity = new $class($row);

		$this->invoke(self::CREATE, [$entity, $this]);

		return $entity;
	}

	/**
	 * @param mixed $id
	 * @return Entity|null
	 */
	public function find($id)
	{
		$row = $this->getTable()->get($id);
		return ($row) ? $this->create($row) : NULL;
	}

	/**
	 * @param array $criteria
	 * @return Entity[]|Collection
	 */
	public function findBy(array $criteria)
	{
		$selection = $this->getTable();
		if ($criteria) {
			$selection->where($criteria);
		}

		return $this->createCollection($selection);
	}

	/**
	 * @return Entity[]|Collection
	 */
	public function findAll()
	{
		return $this->findBy([]);
	}

	/**
	 * @param array $criteria
	 * @return Entity|null
	 */
	public function findOneBy(array $criteria)
	{
		$row = $this->getTable()->where($criteria)->limit(1)->fetch();
		return ($row) ? $this->create($row) : NULL;
	}

	/**
	 * @param Entity $entity
	 * @throws \Exception
	 * @return bool
	 */
	public function persist(Entity $entity)
	{
		$this->checkEntity($entity);

		return $this->transaction(function () use ($entity) {
			$row = $entity->toRow();
			$isNew = !$row->hasNative();

			$this->invoke(self::BEFORE_PERSIST, [$entity, $this]);

			if ($isNew) {
				$this->invoke(self::BEFORE_PERSIST_NEW, [$entity, $this]);

				$inserted = $this->getTable()->insert($row->getModified());

				// prevents bug in NDTB if inserting own primary non-AUTO_INCREMENT key
				if ($inserted && is_int($inserted)) {
					$inserted = $this->getTable()->where('id', $this->database->getInsertId())->fetch();
				}

				if ($inserted instanceof Nette\Database\Table\ActiveRow) {
					$row->setNative($inserted);
				}

				$return = ($inserted instanceof Nette\Database\IRow || $inserted > 0);

			} else {
				$return = $row->update();
			}

			$entity->refresh();

			$this->invoke(self::AFTER_PERSIST, [$entity, $this]);

			if ($isNew) {
				$this->invoke(self::AFTER_PERSIST_NEW, [$entity, $this]);
			}

			$this->invokeHandlers($entity->onPersist, [$entity, $this]);
			$entity->onPersist = [];

			return $return;
		});
	}

	/**
	 * @param Entity $entity
	 * @throws \Exception
	 * @return bool
	 */
	public function delete(Entity $entity)
	{
		$this->checkEntity($entity);

		return $this->transaction(function () use ($entity) {
			$entity->getEntityValues();

			$this->invoke(self::BEFORE_DELETE, [$entity, $this]);

			$return = TRUE;
			$row = $entity->toRow();
			if ($row->hasNative()) {
				$return = ($row->getNative()->delete() > 0);
			}

			$this->invoke(self::AFTER_DELETE, [$entity, $this]);

			$this->invokeHandlers($entity->onDelete, [$entity, $this]);
			$entity->onDelete = [];

			return $return;
		});
	}

	/**
	 * @param Nette\Database\Table\Selection $selection
	 * @param string $entity
	 * @param string $refTable
	 * @param string $refColumn
	 * @return Collection
	 */
	protected function createCollection($selection, $entity = NULL, $refTable = NULL, $refColumn = NULL)
	{
		return new EntityCollection($selection, $this->getEntityClass($entity), $refTable, $refColumn);
	}

	/**
	 * @param string $table
	 * @return Nette\Database\Table\Selection
	 */
	protected function getTable($table = NULL)
	{
		return $this->database->table($this->getTableName($table));
	}

	/**
	 * @param string|null $table
	 * @throws Exception\InvalidStateException
	 * @return string
	 */
	protected function getTableName($table = NULL)
	{
		if ($table === NULL) {
			if ($this->tableName === NULL) {
				if (($name = static::getReflection()->getAnnotation('table')) !== NULL) {
					$this->tableName = $name;

				} elseif (!$this->parseName($name)) {
					throw new Exception\InvalidStateException('Table name not set.');
				}

				$this->tableName = strtolower($name);
			}

			$table = $this->tableName;
		}

		return $table;
	}

	/**
	 * @param string $entity
	 * @throws Exception\InvalidStateException
	 * @return string
	 */
	protected function getEntityClass($entity = NULL)
	{
		if ($entity === NULL) {
			if ($this->entity === NULL) {
				$ref = static::getReflection();
				if (($name = $ref->getAnnotation('entity')) !== NULL) {
					$this->entity = Nette\Reflection\AnnotationsParser::expandClassName($name, $ref);

				} elseif ($this->parseName($name)) {
					$this->entity = Nette\Reflection\AnnotationsParser::expandClassName($name, $ref);

				} else {
					throw new Exception\InvalidStateException('Entity class not set.');
				}
			}

			$entity = $this->entity;
		}

		return $entity;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	private function parseName(& $name)
	{
		if (!($m = Nette\Utils\Strings::match(static::getReflection()->name, '#([a-z0-9]+)repository$#i'))) {
			return FALSE;
		}

		$name = ucfirst($m[1]);
		return TRUE;
	}

	/**
	 * @param Entity $entity
	 * @throws Exception\InvalidArgumentException
	 */
	private function checkEntity(Entity $entity)
	{
		$class = $this->getEntityClass();
		if (!($entity instanceof $class)) {
			throw new Exception\InvalidArgumentException("Instance of '$class' expected, '"
				. get_class($entity) . "' given.");
		}
	}

	/** @return Nette\Reflection\ClassType|\ReflectionClass */
	public static function getReflection()
	{
		$class = get_called_class();
		if (!isset(self::$reflections[$class])) {
			self::$reflections[$class] = parent::getReflection();
		}

		return self::$reflections[$class];
	}

	// === TRANSACTIONS ====================================================

	/**
	 * @param \Closure $callback
	 * @throws \Exception
	 * @return mixed
	 */
	public function transaction(\Closure $callback)
	{
		try {
			$this->begin();
			$return = $callback();
			$this->commit();

			return $return;
		} catch (\Exception $e) {
			$this->rollback();
			throw $e;
		}
	}

	protected function begin()
	{
		if (self::$transactionCounter[$this->database->getConnection()->getDsn()]++ === 0) {
			$this->database->beginTransaction();
		}
	}

	/**
	 * @throws Exception\InvalidStateException
	 * @return void
	 */
	protected function commit()
	{
		if (self::$transactionCounter[$dsn = $this->database->getConnection()->getDsn()] === 0) {
			throw new Exception\InvalidStateException('No transaction started.');
		}

		if (--self::$transactionCounter[$dsn] === 0) {
			$this->database->commit();
		}
	}

	protected function rollback()
	{
		if (self::$transactionCounter[$dsn = $this->database->getConnection()->getDsn()] !== 0) {
			$this->database->rollBack();
		}

		self::$transactionCounter[$dsn] = 0;
	}

}
