<?php

namespace Artfocus\JetORM;

use Nette;

class EntityCollection extends Nette\Object implements Collection
{

	/** @var Nette\Database\Table\Selection|null */
	protected $selection;

	/** @var string|callable|null */
	protected $entity;

	/** @var string|null */
	protected $refTable;

	/** @var string|null */
	protected $refColumn;

	/** @var Entity[] */
	protected $data;

	/** @var int */
	private $count;

	/** @var int */
	private $countDistinct;

	/** @var array */
	private $keys;

	/**
	 * @param Nette\Database\Table\Selection $selection
	 * @param string|callable|null $entity
	 * @param string $refTable
	 * @param string $refColumn
	 */
	public function __construct(Nette\Database\Table\Selection $selection = NULL, $entity = NULL, $refTable = NULL, $refColumn = NULL)
	{
		$this->selection = $selection;
		$this->refTable = $refTable;
		$this->refColumn = $refColumn;

		try {
			Nette\Utils\Callback::check($entity);
			$this->entity = Nette\Utils\Callback::closure($entity);

		} catch (\Exception $e) {
			$this->entity = $entity;
		}
	}

	/** @return array */
	public function toArray()
	{
		return iterator_to_array($this);
	}

	/**
	 * @return $this
	 */
	public function resetOrder()
	{
		if ($this->selection) {
			// it can't be directly set on the \Nette\Database\Table\Selection (v2.3@beta)
			$builder = $this->selection->getSqlBuilder();
			$builder->setOrder([], []);
		}

		return $this;
	}

	/**
	 * API:
	 *
	 * <code>
	 * $this->orderBy('column', Collection::DESC); // ORDER BY [column] DESC
	 * // or
	 * $this->orderBy([
	 *    'first'  => Collection::ASC,
	 *    'second' => Collection::DESC,
	 * ]); // ORDER BY [first], [second] DESC
	 * </code>
	 *
	 * @param string|array $column
	 * @param bool $direction
	 * @return $this
	 */
	public function orderBy($column, $direction = NULL)
	{
		if ($this->selection) {
			if (is_array($column)) {
				foreach ($column as $col => $d) {
					$this->orderBy($col, $d);
				}

			} else {
				$direction === NULL && ($direction = static::ASC);
				$sqlDirection = ($direction === static::DESC ? ' DESC' : '');

				if ($column instanceof FieldOrder) {
					$this->selection->order('FIELD(' . $column->getField() . ', ?)', $column->getValues());
				} else {
					$this->selection->order($column . $sqlDirection);
				}
			}

			$this->invalidate();
		}

		return $this;
	}

	/**
	 * @param int $limit
	 * @param int $offset
	 * @return $this
	 */
	public function limit($limit, $offset = NULL)
	{
		if ($this->selection) {
			$this->selection->limit($limit, $offset);
			$this->invalidate();
		}

		return $this;
	}

	public function rewind()
	{
		$this->loadData();
		$this->keys = array_keys($this->data);
		reset($this->keys);
	}

	/** @return Entity */
	public function current()
	{
		$key = current($this->keys);
		return $key === FALSE ? FALSE : $this->data[$key];
	}

	/** @return mixed */
	public function key()
	{
		return current($this->keys);
	}

	public function next()
	{
		next($this->keys);
	}

	/** @return bool */
	public function valid()
	{
		return current($this->keys) !== FALSE;
	}

	/** @return int */
	public function count()
	{
		if ($this->data !== NULL) {
			return count($this->data);
		}

		if (!$this->selection) {
			return 0;
		}

		if ($this->count === NULL) {
			$this->count = $this->selection->count('*');
		}

		return $this->count;
	}

	/**
	 * @param string $function
	 * @return mixed
	 */
	public function aggregation($function)
	{
		if (!$this->selection) {
			return NULL;
		}

		return $this->selection->aggregation($function);
	}

	/**
	 * @param string $column
	 * @return int
	 */
	public function countDistinct($column)
	{
		if (!$this->selection) {
			return 0;
		}

		if ($this->countDistinct === NULL) {
			$this->countDistinct = $this->selection->count("DISTINCT $column");
		}

		return $this->countDistinct;
	}

	/** @return void */
	private function loadData()
	{
		if ($this->data === NULL) {
			$this->data = [];

			if (!$this->selection) {
				return;
			}

			if ($this->entity instanceof \Closure) {
				$factory = $this->entity;

			} else {
				$class = $this->entity;
				$factory = function ($record) use ($class) {
					return new $class($record);
				};
			}

			foreach ($this->selection as $row) {
				$record = $this->refTable === NULL ? $row : $row->ref($this->refTable, $this->refColumn);
				$this->data[] = Nette\Utils\Callback::invoke($factory, $record);
			}
		}
	}

	private function invalidate()
	{
		$this->data = NULL;
	}

}
